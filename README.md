# Basic setup

## How to use

Clone the repo

Install the dependencies
```
npm install
```

Then either, open Sketch and navigate to `Plugins → react-sketchapp: Basic skpm Example`

Or, run with live reloading in Sketch (need a new sketch doc open).
```
npm run render
```
